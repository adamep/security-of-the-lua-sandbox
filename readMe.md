This repository contains my bachelor's thesis. The structure is as follows:

* **Implementation** --------- the directory with all practical parts of this thesis
    * **OpenMWEscape** ---- the directory with a proof of concept mod for OpenMW
    * **Analyser** ------------- the directory containing the Lua source code of the environment analyser
* **Text** --------------------- the thesis text and LATEX source codes directory
    * **BP_Adamek_Petr_2022.pdf** ------------ the thesis text in PDF format