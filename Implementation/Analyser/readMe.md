This folder contains the implementation of the Automatic Environment Crawler mentioned in the thesis.

Key files are:

 *	`envAnalyze.lua` containing the implementation of the analyser module.
 *	`envDump.lua` containing the implementation of the dumper module.
 *	`functionConf.lua` which describes which functions the dumper uses and allows for easy overloading if needed.

All the other files contain either the internal logic or other utility functions used by the Dump and Analyser modules.

The StoredEnvs folder contains an example of the environment dumped from the OpenMW engine after the first error was fixed.
It also contains an environment dumped from the game Factorio, which utilises a pure State-based sandbox.

All of the code in this folder was created by me and fall under the same licence as is written in the thesis.
```
I acknowledge that my thesis is subject to the rights and obligations stipulated
by the Act No. 121/2000 Coll., the Copyright Act, as amended. In accordance
with Article 46 (6) of the Act, I hereby grant a nonexclusive authorization
(license) to utilize this thesis, including any and all computer programs
incorporated therein or attached thereto and all corresponding documentation
(hereinafter collectively referred to as the “Work”), to any and all persons that
wish to utilize the Work. Such persons are entitled to use the Work in any
way (including for-profit purposes) that does not detract from its value. This
authorization is not limited in terms of time, location and quantity.
```
A common usecase of the entire tool might look as follows:
***
**Dumping**
```lua
util = require('envDump');
print(util.dumpNoWhitespace(util.machineReadableSplit( util.scanEnv(_ENV) ) ))
```
For an existing example of a dumping mod, see the folder DumpingExample.

And later crating a file with these dumped values to be loaded by the analyser. For an example, see the folder StoredEnvs.

The dumped values are structured as a Lua table. 
The structure is:
```lua
{
['parents'] = {['<any string identifier>'] = {['<source String Identifier>'] = {metadata of operation applied to this value} } }
['index'] = {['<type>'] = {['<uniqueStringIdentifier>'] = 'result of tostring OnOriginalValue'} }
}
```
***
**Analysing**

The workflow of the analyser looks as follows. It is to be ran in an interactive interpreter of Lua, the version of which should not matter. Only LuaJIT 2.1(beta) and Lua 5.2 have been tested.
```lua
dofile('envAnalyze.lua'); --This loads the analyser functions.
loadEnvs('pathToEnvironments.lua'); --such as `loadEnvs('StoredEnvs\\OpenMWEnv.lua');` This loads the stored environment(s) into the analyser

paths('__gc'); --Here I specify a specific index which I wish to search for. If '__gc' is a unique identifier as found in the index table, it is used. 
			   --Otherwise a lookup is attempted in the values portion of the 'index' table to find a unique identifier which was tostring-ed to this value
			   
keyUses('__gc');
```
A useful command for analysis is the following which initialises the working set with all tables which are contained in both environments:
```lua
analyzisList = bucketSets(LHS_env.index.table, RHS_env.index.table).both or {}; --load intersection of tables
```
This set is just a convenient way to handle multiple values. It is by default loaded with the intersection of tables in both of the loaded environments.


For a complete list of functions, see the bottom of the envAnalyze.lua file.


Analysing one of the sandboxes mentioned in my thesis looked as follows:
```lua
dofile('envAnalyze.lua')
sbx = dofile('sbx.lua')
env1 = sbx.protect([[
local util = require('envDump') ;
return util.machineReadableSplit( util.scanEnv(_ENV));]]
, {env = {require = function(str) local k, env = debug.getupvalue(debug.getinfo(2,'f').func,1);return assert(loadfile(str..'.lua','t',env))() end }, quota = false});
loadEnvs('return env1()', true);

add(LHS_env.index['function'])
pathsAll();
```

***
**Other Notes**

Many TODO: notes can be found throughout the code. These most often refer to new fetaures or other potential improvements. 
They may also warn of currently existing bugs which did not hinder the usage of this tool enough to warrant fixing just yet.
