--[[

This file contains a couple of important functions.

The `toStringIdentifier` function attempts to assign each value a unique identifier

util.translateSources takes values directly from the analyser and turns them into a human-readable format. 
Can be used if just dumping some data without the use of the analyser.

util.translateSources_* are used by the analyser and take values in the format which they are serialized into by the dumper.
They also generate a human readable variant of the input.

]]
local conf = require('functionConf');

local insert = conf.insert;
local next = conf.next;
local type = conf.type;
local pairs = conf.pairs;
local tostring = conf.tostring;
local setmetatable = conf.setmetatable;

local sets = require('setUtilities');
local logging = require('genericLogger');
local util = {}
sets.mergeInto(util, sets);
sets.mergeInto(util, logging);

util.enum_source = {specifiedForSearch = 1, key = 2,value = 3,metatable = 4,env = 5, 
					 pairsFunction = 6, pairsTable = 7, pairsValue = 8,
					 ipairsTable = 9, ipairsValue = 10, ipairsFinal = 11, upvalue = 12, upvalueNameOrIndex = 13};
local enum_source = util.enum_source;
util.reverse_enum_source = {"specifiedForSearch", "key" ,"value","metatable","env", 
					 "pairsFunction", "pairsTable", "pairsValue",
					 "ipairsTable", "ipairsValue", "ipairsFinal",
					 "upvalue", "upvalueNameOrIndex"};
local reverse_enum_source = util.enum_source;

local nameDatabase = {};
local reverseNameDatabase = {}; 
--BEWARE, this leads to no memory getting freed unless the setmetatable works

if(setmetatable) then
	setmetatable(nameDatabase, {__key = 'v'})
	setmetatable(reverseNameDatabase, {__key = 'k'})
end

--global intentionally
--TODO: use string.dump to check for type of function(C/Lua) if applicable.
--TODO: after a free from the weak table, this function will reuse the identifier... FIX if need be.
function toStringIdentifier(key) --this creates a unique ID based on the and the reust of the tostring method. Disallows for conflicts between carefully crafted strings and tables.
	if(type(key) == 'nil') then
		return 'nil__nil'
	end;
	
	if(reverseNameDatabase[key]) then
		return reverseNameDatabase[key];
	end;
	
	local potential = tostring(key) .. "__" .. type(key);
	if(not nameDatabase[potential]) then
		nameDatabase[potential] = key;
		reverseNameDatabase[key] = potential;
	else 
		local counter = 0;
		local original = potential;
		while(nameDatabase[potential]) do
			potential = original .. "_" .. counter;
			counter = counter +1;	
		end;
		nameDatabase[potential] = key;
		reverseNameDatabase[key] = potential;
	end;
	
	return reverseNameDatabase[key];
end;
util.toStringIdentifier = toStringIdentifier;


function util.translateSources(sources)
	local allOptions = 'NOTHING';
	local first = true;
	for _, lastNode in pairs(sources) do
		local lastNodeType = lastNode[2];
		local pathPoint;
		if(lastNodeType == enum_source.specifiedForSearch) then
			pathPoint = tostring(lastNode[1]);
		elseif(lastNodeType == enum_source.env) then
			pathPoint = 'env(' .. toStringIdentifier(lastNode[1]) .. ')';
		elseif(lastNodeType == enum_source.metatable) then
			pathPoint = 'metatable(' .. toStringIdentifier(lastNode[1]) .. ')';	
		elseif(lastNodeType == enum_source.key) then
			pathPoint = toStringIdentifier(lastNode[1]) .. '.key(%1)';	
		elseif(lastNodeType == enum_source.value) then
			pathPoint = toStringIdentifier(lastNode[1]) .. '[' .. tostring(lastNode[3]) .. ']';	
		elseif(lastNodeType == enum_source.pairsFunction) then
			pathPoint = toStringIdentifier(lastNode[1]) .. '.pairs[1]';	
		elseif(lastNodeType == enum_source.ipairsFunction) then
			pathPoint = toStringIdentifier(lastNode[1]) .. '.ipairs[1]';	
		elseif(lastNodeType == enum_source.pairsTable) then
			pathPoint = toStringIdentifier(lastNode[1]) .. '.pairs[2]';	
		elseif(lastNodeType == enum_source.ipairsTable) then
			pathPoint = toStringIdentifier(lastNode[1]) .. '.ipairs[2]';	
		elseif(lastNodeType == enum_source.pairsValue) then
			pathPoint = toStringIdentifier(lastNode[1]) .. '.pairs[3]';	
		elseif(lastNodeType == enum_source.ipairsValue) then
			pathPoint = toStringIdentifier(lastNode[1]) .. '.ipairs[3]';	
		elseif(lastNodeType == enum_source.upvalue) then
			pathPoint = toStringIdentifier(lastNode[1]) .. '.upvalue' .. '[' .. tostring(lastNode[3]) .. ']';	
		end;
		if(first) then
			allOptions = pathPoint
		else
			allOptions = allOptions .. ' || ' .. pathPoint;
		end;
		first= false;
	end;
	return allOptions;
end;


--the following functions are meant to work with nodes nransfered into the machine readable split thingy

function util.translateSources_short(sources)
	local allOptions = 'NOTHING';
	local first = true;
	for _, lastNode in pairs(sources) do
		local lastNodeType = lastNode[2];
		local pathPoint;
		if(lastNodeType == enum_source.specifiedForSearch) then
			pathPoint = '.';
		elseif(lastNodeType == enum_source.env) then
			pathPoint = 'env(%1)';
		elseif(lastNodeType == enum_source.metatable) then
			pathPoint = 'metatable(%1)';	
		elseif(lastNodeType == enum_source.key) then
			pathPoint = '.key(%1)';	
		elseif(lastNodeType == enum_source.value) then
			if(lastNode[3][1] ~= 'string') then
				pathPoint = '[' .. lastNode[3][3] .. ']';	
			else
				pathPoint = '["' .. lastNode[3][3] .. '"]';	
			end
		elseif(lastNodeType == enum_source.pairsFunction) then
			pathPoint = '.pairs[1]';	
		elseif(lastNodeType == enum_source.ipairsFunction) then
			pathPoint = '.ipairs[1]';	
		elseif(lastNodeType == enum_source.pairsTable) then
			pathPoint = '.pairs[2]';	
		elseif(lastNodeType == enum_source.ipairsTable) then
			pathPoint = '.ipairs[2]';	
		elseif(lastNodeType == enum_source.pairsValue) then
			pathPoint = '.pairs[3]';	
		elseif(lastNodeType == enum_source.ipairsValue) then
			pathPoint = '.ipairs[3]';	
		elseif(lastNodeType == enum_source.upvalue) then
			pathPoint = '.upvalue' .. '[' .. lastNode[3][3] .. ']';	
		end;
		if(first) then
			allOptions = pathPoint
		else
			allOptions = allOptions .. ' || ' .. pathPoint;
		end;
		first= false;
	end;
	return allOptions;
end;

function util.isSide(key)
	return (key == 'left' or key == 'both' or key == 'right' )
end;
local isSide = util.isSide

--sources is a list of possible parent Nodes
--soFar is a table with the structure: {type => {array of paths}}
--returns array of all paths regardless of the subtype
local function translateSources_Embed_internal_really(sources, soFar)
	local function nodeToString(node)
		if(node[3][1] ~= 'string') then
			return node[3][3];
		else
			return "'" .. tostring(node[3][2]) .. "'";
		end;
	end

	
	local res = {};
	local insert = function(path) insert(res,path) end
	for _, lastNode in pairs(sources) do
		local lastNodeType = lastNode[2];
		if(lastNodeType == enum_source.specifiedForSearch) then
			insert("__ENV[" .. lastNode[1] .. "]");
		else
			for srcType, pathsToHere in pairs(soFar) do		
				for _, pathSoFar in pairs(pathsToHere) do		
					if(lastNodeType == enum_source.env) then
						insert('getfenv(' .. pathSoFar .. ')');
					elseif(lastNodeType == enum_source.metatable) then
						insert('getmetatable(' .. pathSoFar .. ')');
					elseif(lastNodeType == enum_source.key) then
						insert('getKey(' .. nodeToString(lastNode) .. ',' .. pathSoFar ..  ')');	
					elseif(lastNodeType == enum_source.value) then
						insert(pathSoFar .. '[' .. nodeToString(lastNode) .. ']');	
					elseif(lastNodeType == enum_source.pairsFunction) then
						insert('pairs(' .. pathSoFar .. ')');	
					elseif(lastNodeType == enum_source.ipairsFunction) then
						insert('ipairs(' .. pathSoFar .. ')');	
					elseif(lastNodeType == enum_source.pairsTable) then
						insert('table.pack(pairs(' .. pathSoFar .. '))[2]');	
					elseif(lastNodeType == enum_source.ipairsTable) then
						insert('table.pack(ipairs(' .. pathSoFar .. '))[2]');	
					elseif(lastNodeType == enum_source.pairsValue) then
						insert('table.pack(pairs(' .. pathSoFar .. '))[3]');	
					elseif(lastNodeType == enum_source.ipairsValue) then
						insert('table.pack(ipairs(' .. pathSoFar .. '))[3]');	
					elseif(lastNodeType == enum_source.upvalue) then
						insert('upvalue(' .. lastNode[3][3] .. ',' .. pathSoFar .. ')');	
					end;
				end;
			end;
		end;
	end;
	return res;
end;

--sources is a list of possible parent Nodes
--soFar is a table with the structure: {type => {array of paths}}
--		or the structure {type => {side => {array of paths}}}
--returns {array of all paths to the given type} 
--			OR
--returns {side => array of all paths to the given type}
local function translateSources_Embed_internal(sources, soFar)
	local res = {};
	local firstV = util.nextValue(soFar,nil);
	local secondLvlKey = type(firstV) == 'table' and next(firstV,nil);
	if(isSide(secondLvlKey)) then
		for side,pathMap in pairs(soFar) do
			res[side] = translateSources_Embed_internal_really(sources,pathMap)
		end;
	else
		res = translateSources_Embed_internal_really(sources,soFar)
	end
	
	return res;
end;
--sources is a list of possible parent Nodes
--soFar is a table with the structure: {type => {array of paths}}
--		or the structure {type => {side => {array of paths}}}
--		or the structure {side => {type => {side => {array of paths}}} }
--		or the structure {side => {type => {array of paths}} }
--returns {side => array of all paths to the given type}
--if no sides are ever noted, returns as 
-- 	{array of all paths to the given type}
function util.translateSources_Embed(sources,soFar)
	local res = {};
	local function addSideResults(side, paths)
		if(res[side]) then
			for k, path in pairs(paths) do
				insert(res[side], path);
			end;
		else
			res[side] = paths;
		end
	end;
	
	if(isSide(next(soFar))) then  --streams will be getting split as a lvl 1 key is a side
		for side,actualSoFar in pairs(soFar) do
			local solutionPaths = translateSources_Embed_internal(sources,actualSoFar);
			if(isSide(next(solutionPaths))) then
				for side, paths in pairs(solutionPaths) do
					addSideResults(side, paths);
				end
			else
				addSideResults(side, solutionPaths);
			end
		end;
	else
		res = translateSources_Embed_internal(sources,soFar); --let the splitting commence elsewhere
	end
	return res;
end;


return util;