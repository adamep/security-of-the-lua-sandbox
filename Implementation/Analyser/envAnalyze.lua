--[[
This file contains most of the logic used by the analyser to load and manipulate data loaded from the serialized dumper output.

for usage see the public API towards the end of the file.

]]


--dofile everywhere as reloading is simpler that way
util = dofile('envDump.lua');
-- THIS IS MEANT TO RUN IN YOUR LOCAL ENV. As getting a repl in a sandboxed environment is a questionable endeavour, I have opted for this option instead. 
-- Could most certainly be bent to abide by the conf and be run there as well.
util.mergeInto(_ENV or getfenv(0),util);
--this is intended to be ran locally after the environment has been dumped via the mentioned dump.

LHS_env = LHS_env or {};
RHS_env = RHS_env or {};

stubEnv = {["index"] = {}, ["parents"] = { ["table"] = {},  }};

local nilIndex = 'nil__nil'

local function identifyIndex(sets)
	local s_i = {};
	local i_s = {};
	
	for type, values in pairs(sets) do
		for identify, string in pairs(values) do
			if(s_i[string] or i_s[identify]) then
		--		print("warn, unhandled dupe", identify,s_i[string], string, i_s[identify]);
			end;
			s_i[string] = identify;
			i_s[identify] = string;
		end;
	end;
	
	return s_i, i_s;
end

--TODO: if this is ever plugged into a live ENV, make these two functions depend on toStringIdentifier and friends.
local function translateIndex(index)
	if(not index) then
		return
	end;
	local r = RHS_identify_to_string[index];
	local l = LHS_identify_to_string[index];
	if(r == l) then
		return r;
	elseif(r == nil and l ~= nil) then
		return l;
	elseif(r ~= nil and l == nil) then
		return r;
	elseif(r == nil and l == nil) then
		return;
	else
		print("warning, missmatch", l, r);
		return l, r;
	end;
		
end;
local function translateStr(index)
	if(not index) then
		return
	end;
	local r = RHS_string_to_identify[index];
	local l = LHS_string_to_identify[index];
	if(r == l) then
		return r;
	elseif(r == nil and l ~= nil) then
		return l;
	elseif(r ~= nil and l == nil) then
		return r;
	elseif(r == nil and l == nil) then
		return;
	else
		print("warning, missmatch", l, r);
		return l, r;
	end;
		
end;

local function generateUses(tbl)
	local KeyIndex = {}
	local index = {}
	local starters = {};
	for k,v in pairs(tbl.parents) do 
		index[k] = {};
	end
	for id, parentCandidates in pairs(tbl.parents) do
		for k,parentValues in pairs(parentCandidates) do
			if(not index[k]) then
				index[k] = {}; --for user provided values
			end
			table.insert(index[k], id .. ' = '.. util.translateSources_short(parentValues));
			for key, values in pairs(parentValues) do
				if(values[2] == util.enum_source.value) then
					local keyString = values[3][3]
					KeyIndex[keyString] = KeyIndex[keyString] or {};
					KeyIndex[keyString][id] = true;
				end;
				if(values[2] == util.enum_source.specifiedForSearch) then
					starters[values[1]] = key;
				end
			end;
		end
	end
	for k,v in pairs(index) do 
		table.sort(v)
	end;
	return index, KeyIndex, starters;
end;

local function getUses(k)
	if(areEqual(LHS_uses[k], RHS_uses[k])) then
		return LHS_uses[k];
	end;
	return filterEmpty(bucketSets(LHS_uses[k] or {}, RHS_uses[k] or {}));
end

function directSources(index, restrictSide)
	if(not index) then
		return {};
	end
	return filterEmpty(bucketSets((LHS_env.parents[index] or {}) , (RHS_env.parents[index] or {})));
end;

local function doKill(tbl)
	local values = {};
	for k,v in pairs(tbl) do
		values[v] = true;
	end;
	local res = {};
	for k,v in pairs(values) do
		table.insert(res, k);
	end;
	return res;
end;

function killDupe(tbl)
	for k,v in pairs(tbl) do
		if(type(v) == 'table') then --notrim otherwise
			if(type(next(v,nil)) ~= 'number') then
				tbl[k] = killDupe(v);
			else
				tbl[k] = doKill(v);
			end;
		end;
	end;
	return tbl;
end;

includeRecursion = false;
onlyShortest = true;

local function mergePossible(left, right, both)
	if(left and right) then --try to match strings
		for kl,vl in pairs(left) do
			if(right[kl] and areEqual(right[kl], vl)) then
					left[kl] = nil;
					right[kl] = nil;
					both[kl] = vl;
			end
		end;
	end;

end
local emptyPath = {['both'] = {['nil'] = {''}} };
local logCnt =0;
function allSources(index,ignoreIndexes, restrictSide)
	if(not index) then
		return {};
	end;
	ignoreIndexes = ignoreIndexes or {left = {}, both = {}, right = {}};
	if(ignoreIndexes[index]) then
		assert(false, "recursion on " .. index .. "the source ought to be corrupted somehow");
	end
	ignoreIndexes[index] = true;
	local direct = directSources(index, restrictSide);
	local results = {left = {}, both = {}, right = {}};
	local restrict = {left = 'l', right = 'r'}
	
	local function doSearch(side) 
		local function storeEm(parentName, values)
			if(next(values) == nil) then
				return; --a blocked recursion or something like that
			end;
			local function add(side, paths)				
				if(not results[side][parentName]) then
					results[side][parentName] = paths;
				else
					for k,v in pairs(paths) do
						table.insert(results[side][parentName], v)
					end
				end;
			end
			--actual code here
			if(util.isSide(next(values))) then
				for side, paths in pairs(values) do
					add(side,paths)
				end
			else
				add(side,values);
			end
		end
		--actual code here
		for parentName,sourceTypes in pairs(direct[side] or {}) do
			if(not ignoreIndexes[parentName]) then
				local res = allSources(parentName,ignoreIndexes,restrict[side]);
				if(parentName == nilIndex or res ~= emptyPath) then
					storeEm(parentName,util.translateSources_Embed(sourceTypes, res ));
				end
			elseif ignoreIndexes[parentName] == emptyPath then
				--empty path, ignore. should not happen unless recursion.
			elseif(type(ignoreIndexes[parentName]) ~= 'boolean') then
				storeEm(parentName,util.translateSources_Embed(sourceTypes, ignoreIndexes[parentName]));
			elseif(includeRecursion) then
				storeEm(parentName,{ "recursion[" .. sourceTypes[1][1] .."]" .. util.translateSources_short(sourceTypes) });
			else
				--do nothing
			end;
		end
	end
	doSearch('left');
	doSearch('both');
	doSearch('right');
	local all =filterEmpty(results);
	
	if(not next(all, nil)) then
		ignoreIndexes[index] = emptyPath;
	else
		all.both = all.both or {};
		mergePossible(all.left, all.right, all.both);
		all = killDupe(filterEmpty(all));
		if(onlyShortest) then
			for side, types in pairs(all) do
				for type, paths in pairs(types) do
					all[side][type] = {util.fold(paths, util.nextValue(paths), function(_,path,longest) return (string.len(path) < string.len(longest)) and path or longest end)}
				end;
			end;
		end;
		ignoreIndexes[index] = killDupe(filterEmpty(all));
	end
	
	return ignoreIndexes[index];
	
end;

function currentSelected() 
	return next(analyzisList, nil);
end
local beSilentFFS = false;

local function wrap(globalName,fn, text)

	local singleFunction = function(index) 
		if(index) then
			index = (translateIndex(index) and index) or translateStr(index)   -- just index or string to index 
		else
			index = currentSelected();
		end;
		assert(index, "the key was not decoded into a proper index. Maybe it does not exist");
		if(not beSilentFFS) then
			print( text .. (index));
		end
		local res = fn(index);
		if(not beSilentFFS) then
			if(type(res) ~= 'table') then
				print(res);
			else
				for k,v in pairs(res) do
					if(type(v) == 'table') then
						util.log(k, v);
					else
						print(k, v);
					end;
				end;
			end;
		end
		return res;
	end
	
	if(globalName) then
		_G[globalName] = singleFunction
		_G[globalName .. 'All'] = function(param) assert(not param, "the all variant takes no parameters"); return map(analyzisList,function (k,_) return k, singleFunction(k) end ); end;
	else
		return singleFunction, function(param) assert(not param, "the all variant takes no parameters"); return map(analyzisList,function (k,_) return k, singleFunction(k) end); end;
	end;
end;

function getPaths(index) 
	res = {};
	res.left = {};
	res.right = {};
	res.both = {};
	for side, types in pairs(allSources(index)) do
		for type, paths in pairs(types) do
			for k, path in pairs(paths) do
				table.insert(res[side], path);
			end;
		end
	end
	mergePossible(res.left, res.right, res.both);
	return killDupe(filterEmpty(res));
end;


------------------------------------------------------------------------------------------PUBLIC API BELLOW----------------------------------------------------------------------------------------------------------
--load envs from file described by src or load src as string if srcIsCode is set
function loadEnvs(src, srcIsCode)
	if(not srcIsCode) then
		LHS_env, RHS_env = dofile(src);
	else
		LHS_env, RHS_env = loadstring and loadstring(src)() or load(src)();
	end
	assert(LHS_env or RHS_env, "no environment loaded");
	LHS_env = LHS_env or stubEnv;
	RHS_env = RHS_env or stubEnv;
	
	analyzisList = bucketSets(LHS_env.index.table, RHS_env.index.table).both or {};

	LHS_string_to_identify, LHS_identify_to_string = identifyIndex(LHS_env.index);
	RHS_string_to_identify, RHS_identify_to_string = identifyIndex(RHS_env.index);

	LHS_uses, LHS_key_uses, LHS_starters = generateUses(LHS_env);
	RHS_uses, RHS_key_uses, RHS_starters = generateUses(RHS_env);

	
end;

--The value on top of the current analysis set. Will be used in most other functions if no parameter is specified.
function current()
	util.log(currentSelected());
	return currentSelected();
end;
--list all values in the analysis list
function currentAll()
	util.log(analyzisList);
	return analyzisList;
end;
--return the analysis list
function all()
	return analyzisList;
end;


--save the analysis set for later reloading
--does not persist between lua restarts
--persists between this file reloading
function save()
	__someReallyUninterestingGlobalValue = analyzisList;
end;

--restores the analysis set to a previous state
function restore(table)
	analyzisList = table or __someReallyUninterestingGlobalValue;
	printAll();
end;


--adds a value or a list of values to the analysis list.
function add(index)
	local function doIndex(index)
		index = translateStr(index) or (translateIndex(index) and index) -- string to index or just index
		if(not index) then
			return false;
		end;
		for typ,array in pairs(LHS_env['index']) do
			if(array[index]) then
				analyzisList[index] = array[index];
				return true;
			end;
		end
		for typ,array in pairs(RHS_env['index']) do
			if(array[index]) then
				analyzisList[index] = array[index];
				return true;
			end;
		end
		return false;
	end;
	
	if(type(index) == 'table')then --add children
		for potentialIndex,actualIndex in pairs(index) do
			if(type(actualIndex) == 'table') then
				add(actualIndex);
			else
				add(potentialIndex);
			end
		end	
	else
		print(doIndex(index));
	end;
	
end

--All of the functions here are identified by the first argument and have an alternative with the suffix All. So the first one in the list can be called as ignore() or ignoreALL();
--The All-variants will iteratively be applied to all of the value is the analysis list.
--All of the non-All functions allow for receiving a custom string identifier, which will be translated and the function applied to it rather than to the top of the analysis list.

--removes current selected or all selected from the list.
wrap('ignore',function(index) local res = not not analyzisList[index]; analyzisList[index] = nil; return res; end, 'ignoring ');
--uses of the index. These uses are meant as a list of all operations which are applied to it
wrap('uses',getUses, 'uses of: ');
--list of all paths to a given index.
wrap('paths',getPaths, 'paths to: ');
--a raw dump of the metadata of direct sources of the index TODO: print prettier, please
wrap('sources',directSources, 'sources of:');
--If used as a key, what value does it point to?
wrap('keyUses', function(index) return filterEmpty(bucketSets((LHS_key_uses[index] or {}) , (RHS_key_uses[index] or {}))) end, 'results of using key: ');

--used for silencing the output. Useful for custom chaining
function silence()  
	beSilentFFS = not beSilentFFS 
end
--turn on and off listing of recursion in paths
function listRecursion()
	includeRecursion = not includeRecursion;
end
--turn on and off whether from a single source type only the shortest path should be written out or if the entire tree should be written out.
function includeAll()
	onlyShortest = not onlyShortest;
end