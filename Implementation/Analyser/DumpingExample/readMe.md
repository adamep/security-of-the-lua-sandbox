To avoid unnecessary duplication, the dumper files are not here. Make sure to include them when attempting to use.
To include them, simply copy over all of the `*.lua` files from the parent folder into this one except the `functionConf.lua` file.

After that, simply running the game with this mod will create a dump in the log.


This mod can also be used to test if the accessible garbage collection metamethod vulnerability has been fixed yet. 
If it has not, pressing the `'x'` key while in game will cause the game to crash due to a null pointer dereference.



When trying to load this mod, consult the tutorial in `<ROOT>\Implementation\OpenMWEscape\readMe.md`
The mods should not be tested at the same time as the user might encounter issues due to duplication of the mod name.