
assert(string.lower('S') == 's');
assert(not pcall( function () getmetatable(string).__index.lower = function(...) return 'dummy' end; assert(string.lower('S') == 'dummy') end),
		'readonly table string is vulnerable to metatable exploiting')
assert(string.lower('S') == 's');

print('Everything is fine for readonly metatables');

return {
}