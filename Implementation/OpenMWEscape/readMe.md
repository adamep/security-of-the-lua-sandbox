This folder contains an example mod for OpenMW that asserts if the vulnerabilities of shared libraries are still contained in the game.

All of the code in this folder was written by me and falls under the licence contained in the thesis.
***
**Setting up the game itself**

To test this plugin out, a working build of OpenMW will be needed.

Downloading build artefacts from the gitlab [pipelines](https://gitlab.com/OpenMW/openmw/-/pipelines?page=1&scope=all) should be sufficient for getting a working executable.

The artefacts from https://gitlab.com/OpenMW/openmw/-/pipelines/533567147 have been confirmed to not contain this exploit.

The artefacts from https://gitlab.com/OpenMW/openmw/-/pipelines/513001932 have been confirmed to contain this exploit.

After downloading the executable files, some sort of data files still need to be loaded. Otherwise the game will errou with the cryptic message `unknown variable 'gamehour'`. For this, follow the tutorial at https://openmw.readthedocs.io/en/latest/manuals/installation/install-game-files.html

I loaded the official Morrowind game files, but using the Example Suit should be sufficient as only launching the game is needed.

***
**Loading the plugin**

The following paragraphs describe what needs to be done to install a mod as can be read in https://openmw.readthedocs.io/en/latest/reference/modding/index.html

When in doubt for where configuration files may be located, consult the documentation https://openmw.readthedocs.io/en/latest/reference/modding/paths.html?highlight=config .

To install this plugin, append the following line to the `openmw.cfg` file:
```
data="<PATH TO THIS FOLDER>"
```
It with the path substituted, it may look something like this:
```
data="D:\School\BAKA\OpenMWexploit"
```
Then in `openmw-launcher.exe`, in the Data Files tab enable the `exploitMod.omwscripts` mod.

***
**Testing**

After launching the game and loading a save or creating a new game, this mod will get loaded and run.
After that, the log will contain messages stating whether the game is vulnerable or not.

a safe output may look as follows:

```
[22:09:17.518 I] L1_-1[variableMetatableAssert/player.lua]:	Everything is fine for the string metatable
[22:09:17.519 I] L1_-1[leakingPairs/player.lua]:	Everything is fine for the pairs function
[22:09:17.519 I] L1_-1[leakingMetatables/player.lua]:	Everything is fine for readonly metatables
```
while a vulnerable one:
```
[08:22:51.692 E] Can't start L1_-1[variableMetatableAssert/player.lua]; Lua error: [string "variableMetatableAssert/player.lua"]:3: this pcall was not supposed to be successfull! either the assignment has to crash or the assert has to throw
[08:22:51.692 E] Can't start L1_-1[variableMetatableAssert/player2.lua]; Lua error: [string "variableMetatableAssert/player2.lua"]:1: another sandbox was affected!!!
[08:22:51.692 E] Can't start L1_-1[leakingPairs/player.lua]; Lua error: [string "leakingPairs/player.lua"]:3: pairs has leaked a mutable table
[08:22:51.693 E] Can't start L1_-1[leakingPairs/player2.lua]; Lua error: [string "leakingPairs/player2.lua"]:1: another sandbox has been affected!
[08:22:51.693 E] Can't start L1_-1[leakingMetatables/player.lua]; Lua error: [string "leakingMetatables/player.lua"]:3: readonly table string is vulnerable to metatable exploiting
[08:22:51.693 E] Can't start L1_-1[leakingMetatables/player2.lua]; Lua error: [string "leakingMetatables/player2.lua"]:1: another sandbox has been affected!
```