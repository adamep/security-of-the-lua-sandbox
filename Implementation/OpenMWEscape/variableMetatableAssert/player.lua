
assert(string.upper('s') == 'S', 'to upper does not work properly');
assert(not pcall(function () 
			getmetatable('s').__index.upper = function(...) return 'dummy' end; 
			assert(string.upper('s') == 'dummy') end), 
	'this pcall was not supposed to be successfull! either the assignment has to crash or the assert has to throw')
	
assert(string.upper('s') == 'S', 'to upper does not work properly');

print('Everything is fine for the string metatable');

return {
}