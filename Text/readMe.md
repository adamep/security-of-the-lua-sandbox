The pdf can be created via 
`arara BP_Adamek_Petr_2022`

The thesis itself can be found in the file `BP_Adamek_Petr_2022.pdf`

The introduction abstract and keywords can be found in `BP_Adamek_Petr_2022.tex` other chapters are either included from there or contained within.

