\chapter{OpenMW}
OpenMW\footnote{https://openmw.org/en/} is an open-source game engine that is designed to be compatible with the engine of the game \emph{The Elder Scrolls III: Morrowind}.

In the next version, a new Lua scripting interface will be added using LuaJIT and the C++ Sol2 framework\footnote{https://github.com/ThePhD/sol2}. 

An analysis was done to see if the standard library functions are restricted properly as outlined in this thesis. This includes making unsafe functions inaccessible and having proper isolation between sandboxes on the Lua side. 
An extensive analysis of the provided C++ API was not done except for the \codeHighlight{require} function that was concluded to be safe.

\section{OpenMW Lua Sandbox Design}
OpenMW utilizes a function-based sandbox --- all scripts are run in the same Lua State. While most of the dangerous libraries mentioned in section \ref{Standard library} are not present in the global environment, the few that are can create a lot of issues.

I have incorrectly claimed in an issue\footnote{https://gitlab.com/OpenMW/openmw/-/issues/6694} made for one of the exploits that a malicious global function would not be able to do harm. This is not true. The debug library is included and functions for loading bytecode are not restricted.

Functions provided to the user sandboxes are a well-restricted subset of the unsafe functions.

Compared to official Lua 5.1, the \codeHighlight{pairs} and \codeHighlight{ipairs} functions have been replaced with alternatives implementations which may be overloaded by the \metafield{pairs} and \metafield{ipairs} metamethods respectively.

Individual Lua scripts should be isolated from directly affecting one another except for the explicit interface provided by the engine. 

Standard libraries which are to be shared are loaded into the global environment and shared based using proxy userdata objects to ensure non-mutability.
The \codeHighlight{require} function also returns shared tables protected in the same manner.
The function responsible for establishing this restriction is called \codeHighlight{makeReadOnly}.

\section{Mistakes with makeReadOnly}
The implementation of \codeHighlight{makeReadOnly} was done incorrectly. The original table was getting leaked in multiple ways. This resulted in the global environment and environments of all other sandboxes getting compromised.

All foreign and internal OpenMW scripts loaded from the filesystem are sandboxed. 
Only two types of functions run in the global environment. Functions provided by the host program which contain no usage of these compromised libraries. 
Functions defined in the global scope that also do not interact with any of the compromised libraries. Moreover, these functions have most of their dependencies stored as upvalues.

The section \ref{FunctionBasedSbx} briefly mentions how much an injected function can realistically do. Since no function interacts with the compromised values, the vulnerability resulted in no further exploits.

A mod that asserts whether these vulnerabilities are still present can be found in the medium attached to this thesis.

\subsection{Accessible Metatables}
	%no definite article with metatables, have not been mentioned before.
	The first flaw of \codeHighlight{makeReadOnly} were unprotected metatables. The \codeHighlight{getmetatable} method was left unchanged, and no protection was offered in the form of a defined \metafield{metatable} value in the metatables.
	Since the original table was stored as the \metafield{index} field of the proxy userdata, accessing the original table was a trivial operation.

\begin{lstlisting}
originalTable = getmetatable(wrappingUserdata).__index
\end{lstlisting}

	The metatable of string values was also accessible. It had not been protected at all.

	Multiple solutions to this issue exist. The use of the \metafield{metatable} field could be enforced to ensure protection. This would result in more upkeep down the line.
	As the program keeps sensitive functions in the metatables of other userdata objects, a blanket approach was deemed more appropriate. 

	\codeHighlight{Getmetatable} was restricted to only work for tables and no other value. 

\subsection{Incorrect pairs overload}

	Both the \codeHighlight{pairs} and \codeHighlight{ipairs} method were affected but the latter has the same behaviour so only the former will be described.

	When implementing \codeHighlight{makeReadOnly}, retaining a consistent API by making pairs callable on the wrapping userdata is desirable. 
	The sandbox did not take into account that the standard library \codeHighlight{pairs} function returns the original table. 
	
	By writing out all the calls happening behind the scenes, we get three equal expressions.
	
	\begin{lstlisting}
pairs(wrappingUserdata) 
pairs(getmetatable(wrappingUserdata).__index) 
pairs(originalTable)
	\end{lstlisting}	
	
	In a for loop this looks safe as the second returned value is not accessible, but it can be explicitly retrieved. 
	
	The solution that was agreed upon looks as follows:
\begin{lstlisting}
function pairsForReadOnly(v)
  local next, t, key = pairs(getmetatable(v).__index)
  return function(_, k) return next(t, k) end, v, key
end	
\end{lstlisting}
	This retains the original properties of the pairs function while ensuring the original table does not get leaked.
    
\section{Accessible Insecure Garbage Collection Metamethod}\label{SolFedUp}

\lstset{language=C}
	
Since the raw access methods are accessible in the sandbox, any shared table is mutable even if protected via a metatable.

After all of the previously mentioned issues had been solved, a basic scan of the environment was done to see if this proves to be an issue. Using the previously mentioned tool,  four shared tables were found. Two of those tables were metatables and two were tables contained in them.
The cause was the Sol framework.

The Sol framework does not take into account that making metatables or even metamethods accessible to the code is dangerous. 
All Sol-mapped userdata objects in the project contain this behaviour. Mapping is created via the \codeHighlight{sol::new\_usertype} and \codeHighlight{sol::make\_object} functions.
When these userdata objects are explicitly queried for metatable keys, the associated metavalues will be returned. 
The \metafield{index} metavalue sometimes contains the metatable itself, resulting in the observed behaviour.
Even objects which do not leak the entire metatable leak their metatable fields. Specific examples are given in the issue\footnote{https://gitlab.com/OpenMW/openmw/-/issues/6698} description.

Among these fields is the \metafield{gc} metamethod. The mapping of this method consistently points to the C++ template function:

\begin{minipage}{\linewidth}
\begin{lstlisting}
template <typename T>
int usertype_alloc_destruct(lua_State* L) {
  void* memory = lua_touserdata(L, 1);
  memory = align_usertype_pointer(memory);
  T** pdata = static_cast<T**>(memory);
  T* data = *pdata;
  std::allocator<T> alloc {};
  std::allocator_traits<std::allocator<T>>
    				::destroy(alloc, data);
	return 0;
}
\end{lstlisting}
\end{minipage}
The first value from the Lua virtual stack is to be interpreted as a userdata object. If it is not userdata, \codeHighlight{NULL} is returned. That results in immediate program termination due to a page fault after null pointer dereference.

In case it is a userdata object, Sol internally uses another layer of indirection and gets the actual location of the object in memory. Then it calls the destructor of the object in that memory location in a roundabout C++ way. Unlike how Lua does its mappings, there are no attempts to ensure the userdata was of the correct type in the first place. 

This results in type confusion bugs. Calling another object's destructor on an object is possible. Calling a destructor on an object repeatedly is also possible. The function has no safeguards against this at all.
While most objects in the API contain trivial destructors, making this harmless, this is not always the case. 

Namely \codeHighlight{LuaUtil::LuaStorage::SectionMutableView} that contains an interesting value \codeHighlight{std::shared\_ptr<Section>} as its first member variable can be used.
Repeatedly invoking its destructor results in the Section object being freed and destroyed while still being used by other parts of the program.

Some of the other structs can be used to create arbitrary in place of the shared pointer. 
For example, the \codeHighlight{LuaUtil::Vec4} object allows for setting arbitrary values to the 4 internal floats.
In theory, an attacker could forge the 16 bytes in the vector in such a way that they contain meaningful values (OpenMW makes no attempts to mitigate issues mentioned in \ref{ASLR}) which then get the \codeHighlight{Section} destructor called upon them and the memory gets freed. 

In practice, this is more difficult as the \codeHighlight{std::shared ptr} contains another layer of indirection and the destructor of \codeHighlight{Section} is not trivial.

As of now, making the game crash at will using this method is possible. 
Freeing data structures and then continuing using them, bringing about use-after-free vulnerabilities, is also realistic.
No method of exploiting this in a meaningful way purely from Lua was found.

At the time of writing this thesis, the issue has not yet been resolved.

\lstset{language={[5.2]Lua}}