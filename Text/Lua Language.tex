
\chapter{Lua}
This chapter covers basic information about the language, its features, and concepts useful for creating a sandbox. 
Lua versions 5.1--5.4 have been checked and relevant differences between the versions will be mentioned. 

Lua is designed to be an embedded language, meaning it is to be used to extend an application rather than to be used to make a program using only it.
The language is heavily reliant on the API provided by the application, as even the standard library functions must be explicitly loaded by it.
\cite{LuaDesign}

The program in which Lua is embedded is referred to as the host program.

There are two main APIs. The language API, which is provided to the program running in Lua, and the C API, which is used to interact with the interpreter from the host program.

The language can be embedded via either static linking or linking against a shared library. Both options provide the same functionality. 
	
The PUC-Rio\footnote{https://www.lua.org/} implementation of Lua will be referred to as the official implementation. 
Other interpreters generally attempt to be compatible with PUC-Rio's C API and Lua libraries, not only the language syntax and semantics, while often providing their extensions.

%the proposed 'the compiled bytecode' and 'The Lua code' are to be kept without a definite article as they are not definite. They have not been mentioned in the past and are generic
All the interpreters mentioned in this work are composed of a language compiler and a virtual machine.
Lua code is compiled into bytecode which is later executed by the virtual machine. The interpreters allow for loading of pre-compiled bytecode but provide no 
guarantees when executing intentionally maliciously crafted bytecode. \cite{LuauSandbox, Lua5.4} 
 
It has been shown that executing malicious bytecode is a serious security vulnerability that may lead to arbitrary code execution as mentioned in section \ref{bytecodeAttack}.
It is either assumed or outright claimed \cite{LuauSandbox} that the bytecode created by the provided compiler will always be safe. 

This is an application of the principles of Software Fault Isolation. The assumption here is that the compiler will inject appropriate restrictions
into the bytecode upon compilation. It also implies that arbitrary changes to the values the virtual machine operates with during the execution could lead to further exploits, if the checks are not set up to handle this.

The Lua manual does not explicitly warn about the dangers of untrusted bytecode: the 5.1 manual \cite{Lua5.1} does not mention it at all, 5.2 \cite{Lua5.2} mentions that all verification of it has been removed and is insecure, and both the 5.3 \cite{Lua5.3} and the 5.4 \cite{Lua5.4} manuals merely claim it may crash the interpreter. 


\section{Language features}
	
Lua is a dynamically typed language with no compile-time type checking. Each function has to check the validity of its arguments manually. Even the argument count is not checked.
	
After embedding into a program, the language runs in an environment called a \emph{State}. This is an object representing the interpreter in which the code is then loaded and executed.
Many APIs are provided, allowing for nigh-arbitrary access to all data and internal values of the interpreter. 
	
A State is designed to only work with a single thread at a time. If multi-threading is desired, such programs have to be split into multiple States akin to creating additional processes on an OS. The problem is, transferring values between States is not trivial.
To put it simply, values cannot be passed into the new environment by reference and instead must be copied with all the limitations and benefits it brings.
	
Not all language features are covered in this section, merely a subset of them considered relevant to this work.  
For example, Lua 5.4 introduces constant local variables which are not considered relevant for sandboxing as they are only local. 

\subsection{Values and their types}
	`\textit{All values in Lua are \textbf{first class values}. This means that all values can be stored in variables, passed as arguments to other functions, and returned as results.}' \cite[ch. 2.2]{Lua5.1}
	
	Some values are very simple.
	The default value of all variables is \emph{nil}. This is usually considered an absence of a value. As far as trivial data types are concerned, there is also \emph{number}, \emph{string}, and \emph{boolean}.
	Lua strings are immutable and are not terminated by zero --- they can contain any values. They are closer to constant byte arrays than to strings in this manner.
	\cite[ch. 2.2]{Lua5.1}
	
	Other values have more complex semantics.
	
	Lua does not have C-like arrays, \emph{tables} are used instead. Tables are associative arrays in which keys and values can be of any type except nil. \cite[ch. 2.2]{Lua5.1}
	
	\subsubsection{Functions}
	\label{Closures}
	\emph{Functions}, whether provided by the host program or Lua are treated the same way in the language. The C API considers them to be distinct types.
	All functions can take an arbitrary number of arguments and may return as many values as they wish. \cite[ch. 2.5.8]{Lua5.1}
	
	All Lua functions may have local variables bound to the current scope and \emph{upvalues} which are references to variables in the enclosing lexical scope.
	Variables not bound to any local or upvalue are stored in the function environment, which may be shared between functions. \cite[ch. 2.6]{Lua5.1}
	
	All executable code is loaded as a function. When loading a file, it is compiled as a vararg function which may then be invoked. 
	In the manual, this executable block of code is called a `chunk'.

	A function's environment is a table that can be manipulated in the same way any other table can. The exact implementation changed between Lua versions
	5.1 and 5.2. It used to be a magical value associated with all functions. 
	In version 5.2 the concept got simplified. Instead, the environment of a function is always bound as its first upvalue under the name \_ENV. \cite[ch. 2.3]{Lua5.1}\cite[ch. 2.2]{Lua5.2}

	The global environment is a table which the State keeps a track of and many functions by default operate on it. Alternatively, in version 5.1, this was a table which was bound to the current thread rather than being universally global. Unless explicitly changed, the global environment is still stored in the \_G variable and may be equal to the \_ENV value.  \cite[ch. 2.3]{Lua5.1}\cite[ch. 2.2]{Lua5.2}

	It is assumed that both local variables and upvalues in a function cannot be retrieved or affected by outside sources. 
	
	\subsubsection{Threads}
	Another value type is coroutines referred to as \emph{threads}. These may wrap a Lua function \cite[ch. 6.2]{Lua5.2} or (since version 5.3 \cite[ch. 6.2]{Lua5.3}) any function. 
	
	As has been mentioned before, this does not allow for executing code in a single State from multiple system threads at once.
	
	\subsubsection{Userdata}
	%when it comes to enumeration Lastly is a synonym of Finally. But I don't particularly care so changed it shall be. The comma was missing
	Finally, Lua has \emph{userdata} values. These represent custom values, and their functionality depends purely on what the host program defines them as. 
	There are two subtypes, \emph{light userdata} and \emph{full userdata}. Assumptions are made that userdata values cannot be created or modified directly in Lua, to ensure the integrity of data in the host.
	\cite[ch. 2.1]{Lua5.2}
	
	Light userdata is just a pointer with no additional associated values. Full userdata is a block of memory allocated by Lua which will be garbage collected by it eventually.
	Lua provides no way to modify or create these values --- this can only be done by the C API.
	Full userdata may also have Lua values associated with them. These had been called environment in Lua version 5.1 but will be referred to as \codeHighlight{user values} as has been the case since. \cite[ch. 2.1]{Lua5.2}
	
	When referring to userdata in this thesis, full userdata are meant as these values carry very important properties, unlike light userdata.
	
	\subsubsection{Value References}
	 `\textit{Tables, functions, threads, and (full) userdata values are \textbf{objects}: variables do not actually \textbf{contain} these values, only \textbf{references} to them. Assignment, parameter passing, and function returns always manipulate references to such values; these operations do not imply any kind of copy}.' \cite[ch. 2.1]{Lua5.4}
	 
	 If any of the above-mentioned \codeHighlight{objects} get passed into two separate sandboxes, the objects may be used for communication between them. This may be both desirable and undesirable as will be later discussed.	 
	 The exact extent to which values may be modified and to what effect is covered in section \ref{valueModifications}.
\subsection{Errors}
	Lua allows for throwing of errors. These errors work similarly to exceptions in other programming languages.
	In Lua, no dedicated object that must be thrown exists --- any value may be thrown. The official interpreter and all its libraries only ever throw strings, but this is only by convention. \cite[ch. 2.3]{Lua5.2}
	 
	Instead of a try-catch block, Lua utilises special calls in \codeHighlight{protected mode}. These are interacted with as regular Lua function calls. They return a boolean, indicating whether an error occurred, and either the error value or all other function return values. From Lua, protected calls are made using \codeHighlight{pcall}. \cite[ch. 2.3]{Lua5.2}
	
	Explicit throwing is done using the \codeHighlight{error} function. \cite[ch. 2.3]{Lua5.2}
	
	Errors may be used for enforcing that a Lua function is forcibly terminated while leaving the interpreter in a consistent state.
	They may be also be used maliciously to subvert control flow at key moments or to attack error handlers. When implementing a handler, it is important not to assume the error will always be a string.

\subsection{Metatables}
	All values in Lua can have an associated \emph{metatable} but only tables and full userdata have individual metatables. All other values share a metatable with all other values of the same type. \cite[ch. 2.4]{Lua5.4}
	
	Values in the metatable are referred to as \emph{metavalues} or \emph{metamethods} if they are functions. 
	A metatable is an ordinary table that is queried during some specific operations, most commonly if the operations were not defined on the value it is associated with, but exceptions exist. 
	
	Each operation is associated with a unique string key, for which the metatable will be queried using \emph{raw access} --- access which ignores metatables associated with the metatable. The standard keys are always preceded by two underscores. \cite[ch. 2.4]{Lua5.4}
	
	The use of metatables is not restricted to the core interpreter. Some functions may optionally respect custom metavalues. This can be done freely and carries no inherent impact unless a collision in the keys occurs.
	
	One such case is the \codeHighlight{getmetatable} method which becomes restricted if the \metafield{metatable} metavalue is defined for the object operated upon. 
	\cite{Lua5.1,Lua5.4}
	
\subsection{Notable Metavalues}\label{MetatableDangers}

	In the context of sandboxing, most of the outlined metavalues are not particularly interesting. They allow for leaking very little data. 
	The \metafield{add} operator is only invoked if the + operator would otherwise fail, as either of the arguments is not a number \cite[ch. 2.4]{Lua5.4}. This is meaningless if 
	argument checking is done based on types. 
	
	Potentially, a function which does not check the types of its arguments and instead use errors to ensure correct execution may be affected.
	
	Most other metavalues work the same way.

	\subsubsection{Garbage Collection}
	%finalization and finalisation are equivalent and both are accepted spellings.
	If a userdata or table object is assigned a metatable with the \metafield{gc} metavalue set, the object is marked for finalization. 
	Just before the object is garbage-collected, the metatable of the object is queried for this metavalue. If this metavalue is a function, it is called with the to-be-collected object as the only argument. \cite[ch. 2.5.1]{Lua5.3}
	
	In version 5.1 this operation is only supported for userdata and the metatable does not have to contain the metavalue when being set, it may be defined later.
	\cite[ch. 2.10.1]{Lua5.1}\cite[ch. 8.3]{Lua5.2}

	As this metamethod operates with to-be-deleted objects, it is assumed to be able to deallocate objects or otherwise affect references. Care needs to be taken that this method never leaks to any sandbox
	or is resilient to being called with arbitrary values and the values are then kept in a safe state. Otherwise, errors such as use after free, double free, or more could arise.
	
	Similar to this are to-be-closed local variables defined in Lua 5.4 with the \metafield{close} metamethod. \cite[ch. 3.3.8]{Lua5.4}

	\subsubsection{Hiding Real Values}	
	
	The \metafield{newindex} is used whenever key-value pair assignment is attempted to a non-existent key of a table or assignment to a non-table value.
	The \metafield{index} is used whenever key indexing is attempted on a table with the key not present or on any non-table value. \cite[ch. 2.4]{Lua5.4}
		
	These two metamethods allow for restricting all writes and reads to a table. They can be bypassed by using raw access, however. 
	They see large usage in the implantation of read-only objects.
	
	Lua also allows for setting these metavalues to tables instead of functions. In that case,  rather than calling a method, the corresponding operation is done on the stored table.

\subsection{Dangers of Shared Metatables}	

	If write access to a metatable is acquired, all metavalues may be changed. This in turn indirectly affects all values which have this metatable set.
	If an operator were to be replaced, the results may be spoofed, and the other argument retrieved and used arbitrarily.
	
	Changing the \metafield{gc} value will lead to all the objects being eventually leaked upon garbage collection.
		
	Even only being able to read a metatable directly carries security implications as the metamethods may otherwise carry some assumptions about the objects they are called with.
	
\section{Sandboxing Mechanisms in Lua} \label{LuaSandboxingMechanisms}
The Lua sandbox can be interpreted as the State which contains and executes all code in which Lua is contained. 
An alternative approach can be taken using purely language features. The former will be referred to as a state-based sandbox and the latter as a function-based sandbox.

Both approaches are not mutually exclusive and have their benefits and downsides. Neither is trivial to implement and keep isolated if a person does not know what they are doing and wishes to expose complex APIs to
the sandbox.

A trivial sandbox with no provided APIs can be constructed very easily. Interacting with such a sandbox would be tedious and even basic, safe functionality of Lua would be restricted.
%actually the AND here is very much intentional. A simple sandbox which cannot communicate with the outside world is trivial. Just don't load any libraries and you are done. YOu can't exactly communicate with it but that is besides the point. 
\subsection{State-based Sandbox}\label{StateBasedSbx}
	\begin{figure}
	    \def\svgwidth{\columnwidth}
    		\input{State.pdf_tex}
		\caption[The state-based sandbox architecture]{\textit{A diagram showing the architecture of a program using a state-based sandbox. Each State has its own dedicated API. All communication is done through the API.}}
		\label{fig:StateBasedSbx}
	\end{figure}
	The state-based approach isolates all individual components into separate States. 
	This approach makes interaction between components more difficult, as Lua has no mechanisms for sharing data between States. 
	All values which are to be shared have to either be copied or some other complex mechanism has to be made up. 
	This approach allows for optimizations based on multi-threading.
	
	The isolation is only dependent on the language not being able to access values not provided to the State.
	As such, the entire Lua State is treated as an unsafe protection domain, which should have no unrestricted privileged functions accessible.

	Policy control with this sandbox is done exclusively in the host application, potentially replacing some of the standard library functions or removing them outright.
	

\subsection{Function-based Sandbox}\label{FunctionBasedSbx}
	\begin{figure}
	    \centering
	    \def\svgwidth{\columnwidth}
    		\input{Function.pdf_tex}
		\caption[The function-based sandbox architecture]{\textit{A diagram showing the architecture of a program using a function-based sandbox. A single State contains both trusted and untrusted code. The untrusted, restricted code cannot access unsafe functions directly. Restrictions may be implemented both in the host and in Lua.}}
		\label{fig:FunctionBasedSbx}
	\end{figure}
	The function-based approach treats individual functions as protection domains. 	%a great enemy of a pointless oxford comma I see :D

	A function cannot affect anything outside of its environment, upvalues, arguments provided to it and values returned from other functions. 
	Functions created in this environment cannot access anything more than these values. This restriction means the functions returned or otherwise created from a domain
	are also automatically confined to the same domain. 
	
	This has large implications on how much harm functions injected into other protection domains can realistically do, as the function only grants the protection domain access to parameters passed into it.
	The replaced function may also return arbitrary values, but the damage which can be done is severely limited. 
	
	Policy enforcement has to be done by the functions provided to the individual protection domains. It can be done in Lua using techniques from section \ref{functionSandboxing} or in the host program.
	
	This means that a single State may contain privileged protection domains with access to dangerous APIs alongside untrusted user code.
	If incorrectly isolated, the dangerous functions may be leaked or called with incorrect arguments.
	
	Sharing values is very simple with this sandbox, as values can be passed as a function argument or saved in a shared table. This, however, allows for accidental sharing of values. 
	Since shared objects can be modified, this may result in unwanted communication or even attacks between domains.

	This entire sandbox trivially breaks down by making the `debug' library accessible. The library contains functions that allow for accessing and changing all values accessible in a given Lua State.
	

\section{Referencing Variables and Values}\label{valueModifications}
This section talks about how values may be shared between different functions and in turn sandboxes. It forms a basis of what needs to be isolated and what may be shared with what restrictions.
It may also shine some light on issues with copying values between sandboxes.
	
\subsection{Variables}
	A variable refers to an identifier under which a value is stored. The language does not provide a way to create arbitrary references to variables. 

	Local variables may be passed by reference only as upvalues which need to be bound on function creation. 
	Upvalue variables a function contains may be referenced in the same manner.
	
	In version 5.1 no built-in method for changing what variable upvalues are bound to exists.
	From version 5.2 functions for binding together multiple upvalues exist in the form of the \codeHighlight{upvaluejoin} API. 
	No API exists for creating a new upvalue from a local variable in a thread.
	
	In all versions, the debug library and the C API allow for accessing and changing the values of upvalues and locals. 

	If these API are not readily accessible, some security guarantees can be inferred. Because the code of a function cannot be changed after creation, 
	all local variables and upvalues can be assumed to be isolated from any outside tampering.
	
	Global variables cannot be passed by reference individually. The environment of a function has to be shared. This works as if sharing just another table.
\subsection{Values}
	Values other than objects are not considered at all in this section, since they do not get passed as reference and are implicitly copied.
	
	Sharing tables implies all keys and values inside it can be retrieved and changed. 
	Techniques restricting reads and writes to a table exist. 
	Extra care has to be taken when either the keys or values are objects as changing them changes the state of the table, even though it is not directly modified.
	
	Dangers of functions passed as a reference depend on the restrictions placed upon a sandbox. The bytecode may not be changed at all, though it may be retrieved. 
	The environment and upvalues may be retrieved and changed depending on provided functions.
	
	Threads, coroutines, including the current running thread, represent the entire running stack. It is possible to retrieve and manipulate all functions on its stack. 
	In this context, the function locals may also be retrieved and modified.
	
	Full userdata have no inherent properties. Their use is defined by their metatable or specific program-defined functions which take userdata. 
	The user values bound to these objects have to be shared explicitly via the C API. The values which are actually stored in the memory block may only be changed by the C API.
	
	The individual metatables of tables and userdata also get shared by the virtue of the objects being shared.

\section{Read only values}\label{RO-val}
This section is only relevant for a function-based sandbox, as a state-based sandbox has no need for additional restrictions on shared values --- all shared values are copied.

In the context of a function-based sandbox sharing values also has to be done. Sharing an object, which is passed by reference, without it being changed maliciously is important. 
One option is to copy over all values. This operation is expensive and not trivial. This leaves the option of somehow ensuring that these shared values are immutable.

This is very straightforward in the case of threads, functions and userdata. Functions that may manipulate these objects need to be restricted or removed outright.

Tables are a more complicated example. Steps have to be taken to ensure the metatable of a table does not get changed. Then the \metafield{newindex} is defined to ensure no new key/value pairs can be assigned.
This will not stop already existing keys from getting assigned a new value. Proxy objects are used for this.

By creating an empty table, all assignments are routed to the \metafield{newindex} metavalue. To ensure values can still be retrieved, the original table is set as the \metafield{index} metavalue.
The danger of this approach comes in the form of raw access. Raw access methods can and will change the values of the proxy table. If these methods are accessible, a proxy table has to be created for each sandbox individually.

Alternatively, a method can be created in the host program which will provide a proxy userdata object. This object is guaranteed not to be mutable and may be shared freely.
It is vital that the function creating the userdata cannot be used to change the metatables or other properties of unrelated userdata. Otherwise it could be used to break the assumption about mutability of userdata from Lua.

Userdata objects are by definition immutable. Their metatables may still be retrieved and potentially changed. This has to be restricted. Proxy tables will not work in this case as metavalues are queried for using raw access. So, for sensitive metatables the access has to be restricted outright.

\section{Applying Resource Limits}
For many applications, restricting how long a particular program may run is a valuable option.
This prevents needless wasting of system resources for malfunctioning or actively malicious components.

\subsection{Restricting operation time}
	A naive implementation might think this can be trivially done by setting a hook. Lua allows for setting a hook that will fire an event on some actions such as calling a function or
	after a set number of lines. While this can effectively prevent the code from running for too long it has its flaws. If a C function which takes a very long time is found, the time per line count will be much larger.
	
	Even if that were solved, the issue of how to recover from the error comes. If the sandbox has access to pcall a single error will not be sufficient in returning to normal context as the error will simply be caught by the sandbox.
	Repeated throwing of errors or restricting of protected mode calls is required. 
	
	Restricting protected calls may be hard, as unrelated functions inside the sandbox may internally use protected calls.
	
\subsection{Restricting memory allocations}
	Memory allocation restrictions can only be done per State in Lua. This is done using a custom allocator which counts the total memory used and refuses to allocate above the limit. 
	
	Lua simplifies the counting by using a \codeHighlight{realloc}-like API that additionally passes the original block size as an argument.
	

\section{C interface }\label{CAPI}
The C API communicates with Lua using a virtual stack. When a function is invoked from Lua, it has a virtual stack assigned which is used for passing variables to Lua.
When a new State is created, values are added to it using the stack.
The programmer has to take care that the virtual stack does not overflow by expanding its capacity as appropriate. 

When checking if a function's Lua stack overflows, the stack space internal Lua functions use does not need to be considered. Lua ensures the stack may grow an extra \codeHighlight{EXTRA\_STACK} spaces for internal use. The extra space ensures that, unless a programmer has made a mistake beforehand, 
the virtual machine will always have enough space on the stack for internal calls, such as calling metamethods.

The API is partially error-prone as the stack prevents the C type-based static analysis from preventing mistakes. All standard Lua values are provided with type-checking retrieval functions, meaning they cannot be confused for data of another type. This does not apply to userdata. 
Lua only has one userdata type while the host program may use userdata for storing multiple different types of objects.

The C API also exposes a Registry, which is a special table associated with a Lua State that may contain confidential values. This table can be potentially passed into Lua as it is just a table.

Lua allows for some type-safety with userdata, if some guidelines are followed. 
The function \codeHighlight{luaL\_newmetatable} cretes a named metatable. 
The name and this metatable are stored in the Registry. 
When creating a userdata object it is assigned this metatable to make it associated with this type. Later \codeHighlight{luaL\_checkudata} can be used to check whether the metatable associated with a given userdata object is the same as is expected.

If the Registry is made accessible to Lua code, or it becomes possible to set a metatable to a userdata object, this type-checking will no longer work.

Neither the lifetime nor the location of Lua objects between calls is guaranteed. If an object needs to be persistent, it must be stored in Lua. The Registry is also used for this purpose. If this convention is not adhered to, this can lead to use-after-free bugs.

While high-level frameworks for Lua exist for creating bindings conveniently, it does not mean that these are safer. As is shown in section \ref{SolFedUp}, it can be quite the opposite.
	
	
\section{Standard library safety}\label{Standard library}
This section covers all of the standard library functions and ways in which they may be abused in the context of both of the mentioned sandboxing approaches.

The functions are grouped by the library which includes them, rather than the name of the table they are located in, as some of the functions are placed in the global environment 
under no other table. Deprecated functions are not included. Only the latest releases of official Lua versions have been checked.

Lua implements all of these functions in C so they could be abused if incorrectly implemented as they are guaranteed to run in an unrestricted context. 
No major mistakes in the function implementation were found, so this section will mostly refer to the intended function semantic being dangerous.
Coercing C functions to run for a very long time is relevant as mechanisms for restricting script runtime are restricted to Lua code itself.

The functions very commonly handle having more arguments than needed by simply ignoring the excess arguments.

\subsection{Base}

	Allowing the \codeHighlight{collectgarbage} function in and of itself does not sound like a big deal, but it greatly increases the attack surface on the garbage collection.
	Numerous bugs concerning garbage collection have been reported in the past \cite{LuaBug} with one allowing for escaping the Lua State in version 5.4 \cite{CVE-2021-44964}.
	Unless a good reason for keeping this function in the sandbox exists, removing it is recommended. Alternatively, restricting it to only accept the `count' argument is safe.
	
	Functions \codeHighlight{dofile}, \codeHighlight{loadfile} allow for loading of arbitrary files. These files may also be interpreted as Lua code and Lua bytecode. Even if the functions were restricted to only load uncompiled code, unrestricted filesystem access is still an issue.
	The \codeHighlight{load} and \codeHighlight{loadstring} functions allow for loading functions from strings. The strings may also be interpreted as bytecode, which needs to be restricted.
	All of these functions also, by default, load the function into the global environment. That needs to be prevented to retain the expectation that all functions created in a protection domain remain in the domain.
	
	The pair of \codeHighlight{get/setmetatable} functions allow for accessing metatables unless \metafield{metatable} is defined. This may have severe security implications for userdata objects and proxy objects.

	The \codeHighlight{print} function allows for arbitrary writes to stdout. This may be unwanted since it can facilitate communication with other programs.

	All raw access methods \codeHighlight{raw*} will have implications for shared tables.
	
	The \codeHighlight{next} method is used to iterate a table using raw access in all versions. If trying to restrict the function, the first return value of the \codeHighlight{pairs} function also has to be restricted as it is this function.
	
	Functions \codeHighlight{ipairs}, \codeHighlight{pairs} allow for iterating over a table using raw access in version 5.1. In version 5.2 they still use raw access but may be overridden by the metamethods \metafield{ipairs} and \metafield{pairs}. Since version 5.3 the ipairs function respects metatables when querying and its metamethod is deprecated.
	
	\label{ASLR}
	The function \codeHighlight{tostring} takes a single argument and converts it to a string. For many types, it returns a value in the form `type: address'. The exact object of which an address is collected varies but if given a good function, getting a pointer to where C functions are allocated in memory is nigh guaranteed. This pretty much breaks or at least reduces the security guarantees which come from using Address space layout randomization. If the \metafield{tostring} metamethod is defined for the argument, it is called instead.
	
	The \codeHighlight{\_G} variable is potentially unsafe if not set to the same value as the function environment. It may also cause issues if the environment is otherwise not directly accessible --- this could lead to the metatable of the environment table getting leaked. 
		
	The \codeHighlight{get/setfenv} functions allow for retrieving and resetting the environments of functions. They are only present in version 5.1 but have severe implications if not restricted at all. They can provide access to the global environment and more.
	
	The function \codeHighlight{unpack} is using raw access but is only present in version 5.1. It was moved to the table library later.
	
	The function \codeHighlight{newproxy} is undocumented, only present in version 5.1, and breaks the assumption that userdata objects cannot be created from Lua. It does not, however, have other security implications. 
\subsection{Coroutine}
	No dangerous functions have been found in this library. Until version 5.4, when both C and Lua functions may be turned into coroutines, it can be used for detecting the type of a function.
\subsection{Debug}
	As the manual clearly states, this library is not meant to be used in usual Lua code and is meant for debugging only. 
	
	If a pure state-based sandbox is used, it may be possible to include this library as well. Almost all functions in this library break some basic assumption about Lua code. Do not include this library in a function-based sandbox.
	
	In a state-based sandbox, allowing access to this library may not lead to an immediate escape from the sandbox, but extra care has to be taken when interacting with Lua.
	Specifically, the Registry, which should be a safe place to store values, will be compromised. This has other implications, since the Registry also stores a copy of all loaded libraries, meaning that some forms of deletion will still result in data getting leaked. Moreover, no metatable will be secure.
	
	If the debug library is allowed in full, especially when concerning manipulation of local values and upvalues, further research needs to be done to ensure this cannot be used to attack the virtual machine as will be hinted at in section \ref{bytecodeAttack}.
	
	The function \codeHighlight{debug.traceback} may be safe if string-based knowledge of the call stack is not considered essential.
\subsection{Package}
	This package is unsafe in its entirety. It allows for loading of both Lua files and native compiled libraries.
\subsection{IO}
	This library is not to be included in any sandbox. It allows for accessing the filesystem and even launching other programs. 
	Even leaking opened file userdata objects is unsafe as they contain callable functions for reading and writing to the opened file in the metatable.
\subsection{OS}
	The os library is also unsafe except for a few functions. I will not be listing why each function is supposed to be restricted, as it is self-evident. I do not believe it is possible to reasonably restrict these functions aside from removing them from the environment outright.
	
	Only the functions \codeHighlight{os.clock}, \codeHighlight{os.time}, \codeHighlight{os.difftime} should be allowed.
	
	Special care has to be taken with the function \codeHighlight{os.date}. The standard library uses the C functions \codeHighlight{gmtime} and \codeHighlight{localtime} which may not be thread-safe.
	
\subsection{Bit32}
	This library is endemic to the 5.2 version. It was later superseded by native binary operators. It contains no unsafe functions.
\subsection{Utf8}
	Nothing of note was found in this library.
\subsection{Table}
	The tables library is mostly safe. Up until version 5.3 it ignored metamethods of its arguments, 
	meaning that all operations were done using raw access and it could only operate on tables. 
	It only ever operates on numeric keys, but it could be an issue for a function-based sandbox that depends on rawset and rawget not being accessible for numeric keys.
	Rawset can be emulated with repeated calls to \emph{table.insert} and rawget with calls to \emph{table.remove}. 
	The deprecated foreach functions also allow for looping over keys using raw access.
	
	The 5.1 version of \emph{table.insert} does not check if pos has a non-negative value. So the call `table.insert({}, -2147483647, "err")' will result in a long freeze of the application.
	Of the other mentioned interpreters, LuaJit suffers from the same issue even though the call finishes in a reasonable time.  
	
\subsection{Math}
	Not much is to be said about the math library. It is a straightforward wrapper for the C math library. Of note are the \emph{random} and \emph{randomseed} methods 
	which could very well affect other sandboxes or the program itself. As such, they are potentially unsafe for use in both types of sandboxes. 
	Though it is to be noted that the C random functions are not meant to be used for security purposes in any case.

\subsection{String}
	Including this library automatically sets the metatable of all strings to one which has the strings library as the \metafield{index} metamethod. 
	This is significant for security purposes, as this creates another way in which the strings library may be referenced. Alternativelly the fields of the metatable could be modified leading to further issues.
	
	Of interest is the \emph{string.dump} function which dumps the bytecode of a Lua function. This bytecode can be analysed externally to understand the internals of a sandbox.
	Some information is also retrieved if an error occurs while dumping. 
	In all versions, an error while dumping cannot happen unless the function is a C function, thus betraying that this is an interesting function to try to exploit.
%\pagebreak %for the love of god do not forget this is here
\section{Summary}\label{restrictionSummary}
Functions that invalidate basic assumptions about the code need to be carefully restricted. These basic assumptions are: the Registry is not accessible from Lua, metatables of userdata cannot be set from Lua and are made inaccessible if they contain unsafe values, user values associated with userdata are not accessible from Lua, and finally local values, upvalues and environments of functions cannot be changed and retrieved by other functions.

In a true state-based sandbox the assumption about functions is not strictly necessary as far as the language is concerned. The virtual machine may also require that some of these guarantees are not broken. If alternative approaches are taken to verify the integrity of userdata, restrictions on the Registry and userdata may also be lifted. Though care needs to be taken so not only the global environment but also the Registry does not contain any exploitable values.

A sandbox may add additional restrictions, especially for raw access methods, if they would violate the integrity of shared data.

Loading of bytecode needs to be forbidden unless the source is guaranteed to be safe. Functions with access to the filesystem and ones which allow for executing files should also be avoided unless properly restricted. 
