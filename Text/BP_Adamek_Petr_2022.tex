% arara: xelatex: { shell: no, synctex: yes, options: [-interaction=nonstopmode] }
% arara: biber
% arara: xelatex: { shell: no, synctex: no, options: [-interaction=nonstopmode] }
% arara: xelatex: { shell: no, synctex: yes, options: [-interaction=nonstopmode] }

% options:
% thesis=B bachelor's thesis
% thesis=M master's thesis
% czech thesis in Czech language
% english thesis in English language
% hidelinks remove colour boxes around hyperlinks

\documentclass[thesis=B,english]{FITthesis}[2019/12/23]
\usepackage[style=iso-numeric]{biblatex}
\addbibresource{myBib.bib}
\usepackage{xcolor} 
\newcommand{\inlineTodo}[1]{\color{red} \textbf{TODO:} [[#1]] \color{black}}
\usepackage{todonotes}
%\renewcommand{\inlineTodo}[1]{}
%\renewcommand{\todo}[1]{}
%\tolerance=1
%\emergencystretch=\maxdimen
%\hyphenpenalty=10000
%\hbadness=10000

\usepackage{blindtext}
\newcommand{\blind}[1][1]{\textcolor{gray}{\Blindtext[#1][1]}}

\newcommand{\directCite}[2]{`\textit{#1}' \cite{#2}}

\RequirePackage{pdfpages}

\usepackage{listings}

\lstset
{
  language         = {[5.2]Lua},
  showstringspaces = false,
  upquote          = true,
}

\newcommand{\codeHighlight}[1]{\emph{#1}}

\newcommand{\metafield}[1]{\codeHighlight{\_\_#1}}
\usepackage[utf8]{inputenc} % LaTeX source encoded as UTF-8
% \usepackage[latin2]{inputenc} % LaTeX source encoded as ISO-8859-2
% \usepackage[cp1250]{inputenc} % LaTeX source encoded as Windows-1250

% \usepackage{subfig} %subfigures
% \usepackage{amsmath} %advanced maths
% \usepackage{amssymb} %additional math symbols

\usepackage{dirtree} %directory tree visualisation
% % list of acronyms
% \usepackage[acronym,nonumberlist,toc,numberedsection=autolabel]{glossaries}
% \iflanguage{czech}{\renewcommand*{\acronymname}{Seznam pou{\v z}it{\' y}ch zkratek}}{}
% \makeglossaries

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% EDIT THIS
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 

\department{Department of Computer Systems}
\title{Security of the Lua Sandbox}
\authorGN{Petr} %author's given name/names
\authorFN{Adámek} %author's surname
\author{Petr Adámek} %author's name without academic degrees
\authorWithDegrees{Petr Adámek} %author's name with academic degrees
\supervisor{Ing. Josef Kokeš}
\acknowledgements{I wish to thank my supervisor, Ing. Josef Kokeš, who helped me whenever I needed it. I could not have created this work without him.}

\abstractEN{
Lua is an easy to embed language which can be used to extend an application with a scripting environment. 
This thesis focuses on isolation of Lua scripts from sensitive parts of the application.

Sandboxing is commonly used for isolation of components in an application. This work covers some theory behind sandboxes and discusses how to properly implement a sandbox in Lua.
A sandbox based on isolation of Lua functions and a sandbox based on isolation of the entire Lua interpreter are proposed as possible implementations.

An analysis of the Lua language, including the standard library, is performed, focusing on isolation and potential for escaping a sandbox.
A simple tool for crawling and dumping the environment of a Lua function is created. The dumped values can be later analysed in an interactive environment. 
This allows for a comprehensive examination of values accessible from a sandbox.

Some freely available Lua sandbox implementations are analysed to show how the theory described here is used in practice.
The theory is further demonstrated on a flaw found in the Lua sandbox of the OpenMW game engine.
}
\abstractCS{
Lua je programovací jazyk, který lze použít pro rozšíření jiné aplikace o skriptovací prostředí.
Tato práce se zabývá izolací Lua scriptů od citlivých částí aplikace.

Sandboxing se používá pro izolaci dílčích částí aplikace. Tato práce rozebírá teorii sandboxingu a diskutuje o tom, jak správně implementovat sandbox v programovacím jazyce Lua.
Jako možná implementace je navržen sandbox založený na izolaci individuálních funkcí a sandbox založený na izolaci celého interpreteru.

Byla provedena analýza programovacího jazyka Lua a jeho standardních knihoven zaměřená na izolaci a potenciál na únik ze sandboxu. 
Následně byl vytvořen jednoduchý nástroj pro vypsání hodnot dostupných v prostředí funkce a jejich následnou analýzu v interaktivním prostředí. 
Toto umožňuje komplexní průzkum hodnot dostupných ze sandboxu.

Také byly provedeny analýzy několika volně dostupných implementací sandboxů pro ukázku, jak se navržené teoretické konstrukty v praxi používají. Nakonec je ukázána chyba v implementaci sandboxu v rámci herního enginu OpenMW.
}
\placeForDeclarationOfAuthenticity{Prague}
\keywordsCS{programovací jazyk Lua, sandboxing, language-based security, language-based sandbox}
\keywordsEN{Lua programming language, sandboxing, language-based security, language-based sandbox }
\declarationOfAuthenticityOption{5} %select as appropriate, according to the desired license (integer 1-6)
% \website{http://site.example/thesis} %optional thesis URL


\begin{document}
% \newacronym{CVUT}{{\v C}VUT}{{\v C}esk{\' e} vysok{\' e} u{\v c}en{\' i} technick{\' e} v Praze}
% \newacronym{FIT}{FIT}{Fakulta informa{\v c}n{\' i}ch technologi{\' i}}

\setsecnumdepth{part}
\chapter{Introduction}
\directCite{We use the term \textbf{sandboxing} to describe the concept of confining a helper application to a restricted environment, within which it has free reign.}{GoldBerg}

Or as \cite{SystematicSandboxingAnalyzis} define a sandbox, `\textit{An encapsulation mechanism that is used to impose a security policy on
software components.}'

Sandboxes are a common part of all computer systems where not all parts can be trusted.
Whether they be virtual machines, an operating system or a web browser, all the mentioned tools use sandboxing in one way or another. \cite{SystematicSandboxingAnalyzis}

Sandboxing enforces that all applications and users can do only what they are allowed to, restricting access to other applications and resources.
In the context of an operating system that can mean, e.g., that a process is only allowed to open files which the user has rights for.

Sandboxing allows us much more freedom without the worry about safety we would have otherwise. 
Programs that use sandboxing allow for installing any plugins and modifications without worrying that they might be malicious,
as the sandbox should prevent them from affecting and accessing anything important. 
But since we give up our security precautions in favour of using the sandbox it is vital for it to be correctly implemented.

This thesis will summarize some commonly used sandboxing techniques and describe how sandboxes are constructed in Lua and other programming languages.

Using this knowledge, different ways of constructing a simple sandbox using Lua will be described. 
The standard libraries provided by the language will be analysed for harmful functions and recommendations for restraining these functions will be given.
Any other known vulnerabilities will also be mentioned.

Later, multiple sandbox implementations will be shown to serve for comparison with the implementation and restrictions proposed in this thesis. Ideally, an attack on one of the sandboxes will be shown to demonstrate mistakes to be wary of.
\setsecnumdepth{all}
\input{Sandboxing general}
\input{Lua language}
\input{Exploiting Lua}
\input{OpenMW}

\chapter{Suggested Improvements}

Possible improvements of sandboxing in Lua can be generally split into two types. One option is improvements done to the language itself on an interpreter level, the other is improvements made by sandbox implementers.

As of now, the language can be used for sandboxing, but the potential ways a sandbox may be implemented are not documented. Pitfalls which have to be avoided when designing a sandbox also are not listed anywhere and each sandbox implementation has to pave its own way. 

\section{Sandboxes}

	Sandbox implementations tend to not give exact specifications of why and how the sandbox works. 
	When the user does not know why a restriction was placed onto a function or why a function was removed, the same behaviour may be brought back by another function.
	A sandbox which cannot be modified, while useful for basic isolation, is not flexible enough for many purposes.
	
	Mentioning that using a state-based sandbox is a possibility would also be very useful. For example, the only semi-official page concerning sandboxes\footnote{https://lua-users.org/wiki/SandBoxes} makes no not of such an approach. 
	
	The page gives some good recommendations for creating a function-based sandbox but fails to show the dangers of functions reliably. For example, the \codeHighlight{rawget} function is marked unsafe while the function \codeHighlight{next} is regarded as fine even though it can be used to achieve the same functionality. 
	
	No such semi-official page exists for Lua versions later than 5.1. 
	
	The getmetatable function is also often regarded as universally unsafe, while that is not necessarily true for a state-based sandbox. By allowing its usage at least for tables, many commonly used features of the language become possible to use. 
	
	A list of potentially unsafe functions on its own is meaningless without an understanding of which guarantees the sandbox requires. Instead, such a list has to be specified for each sandbox implementation.

	A sandbox could also be described very precisely by an interpreter itself. If sandboxign were a priority, the interpreter could keep a subset of the standard library that is guaranteed not to directly interact with the OS and retains guarantees which the virtual machine requires. 

\section{The Language}

	Modifying the language so sandboxing becomes a bigger priority is possible, some parts of the design are inherently unsafe.
	
	The most notable of these is the need for garbage collection metamethods being accessible from Lua. As of now, the language provides no way to guarantee that would ensure a value, which is loaded in a State, cannot be reached from any code in the State. As a result, all metatables have to be carefully secured to ensure no vulnerable methods get leaked. The garbage collection methods are an especially bad case of this, as they may often leave the destroyed objects in an unusable state.
	
	By adding a way to keep these functions only for the C API, this issue could be prevented entirely.
	
	An issue for all function-based sandboxes are shared tables. Currently, all sandboxes have to implement their own methods for making them read-only. This can be an error-prone operation as the issues in OpenMW show. By adding read-only as a part of the interpreter, much of this complexity could be avoided.
		
	Lua version 5.1 does not have any way in which functions can be safely loaded from a string while being confined to the same function environment. This has been resolved in later versions but remains an issue for interpreters using this version.
\section{Existing Solution}	
	The Luau interpreter solves many of the mentioned issues.
	
	The changes Luau did solve the most obvious issues a sandbox in the official Lua interpreter has to contend with.

	The option to make tables read-only not only by restricting each function which may be using raw access and having to use proxy tables, but instead being done on an interpreter level is extremely helpful. It takes all the complexity of ensuring the metatable nor any properties of a table can be changed and transparently locks the table down without having to resort to complex interactions which may be broken by accidentally exposing a new API. This essentially takes essentially multiple different API and combines them to only have to care for one function which may break everything.

	Luau also removes the dangerous \metafield{gc} metamethod from the API in all forms. While it prevents the metamethod from being callable for tables, that is not an issue for Luau, as the only version it is officially fully compatible with is 5.1 \cite{LuauCompat}. 
	
	The interpreter does not solve the issue in a more generic way. Other metamethods may still be leaked into the sandbox.

\chapter{Conclusion}

This thesis explores common sandboxing techniques and how they may apply to a sandbox in Lua. Based on this, different ways of constructing a sandbox are described. 
 
Lua lets us create a language-based sandbox using functions and overwriting of variables.
It allows us fine-grained control of functions and resources accessible to the user code, which is then run in isolation.
That allows us to control which variables are accessible to different functions.
We can thus segregate the untrusted user-provided code from our own trusted code while controlling which functions and resources the user can access.
 
Alternatively, a Lua State-based sandbox may be implemented. This approach requires preventing dangerous functions from getting loaded into the Lua State in the first place. The entire Lua State is assumed to contain malicious code.
 
Lua does not, however, contain mechanisms for access control on a more fine-grained level --- such as which files should be accessible. 
That has to be implemented on a separate layer by individual programs embedding it.


The Lua standard libraries were analysed for potentially dangerous functions which may leak data or otherwise compromise a sandbox. Based on this knowledge, a tool that automatically crawls all values a Lua function may access was created. It was successfully used to show vulnerabilities in the implementation of a Lua sandbox of the OpenMW game engine. 

% \bibliographystyle{iso690}
% \bibliography{myBib}

\printbibliography

\setsecnumdepth{all}
\appendix

\chapter{Acronyms}
% \printglossaries
\begin{description}
	\item[API] Application programming interface
	\item[OS] Operating system
\end{description}


\chapter{Contents of enclosed CD}
\begin{figure}
	\dirtree{%
		.1 readme.md\DTcomment{   the file with CD contents description}.
		.1 Implementation\DTcomment{   the directory with all practical parts of this thesis}.
		.2 OpenMWEscape\DTcomment{   the directory with a proof of concept mod for OpenMW}.
		.2 Analyser\DTcomment{   the directory containing the Lua source code of the environment analyser}.
		.1 Text\DTcomment{   the thesis text and \LaTeX{} source codes directory}.
		.2 BP\_Adamek\_Petr\_2022.pdf\DTcomment{   the thesis text in PDF format}.
	}
\end{figure}

\end{document}
