
\chapter{Sandboxes and Sandboxing Techniques}

Sandboxes are based on the concept of \emph{protection domains}. 

A protection domain is a collection of access rights to objects. An access right is the ability to perform a specific operation, e.g., reading a file. 
Objects are specific software (such as files or processes) and hardware (such as CPU instructions, drives and printers) resources. Code running within a protection domain may
only operate on objects using the access rights of the domain. \cite[p. 627--628]{OS9th}

Communication and switching between domains is restricted to ensure that the restrictions cannot be bypassed.
What concept the domain encapsulates is not always the same. Users, individual processes, or even individual functions may be regarded as a domain depending on the system.
\cite[p. 628--629]{OS9th}

Each domain can be thought of as an individual sandbox. To ensure it works well, two problems must be handled --- isolation and policy enforcement.
%SystematicSandboxingAnalyzis will not be cited as it does not bring anything new. It describes mostly how it came to the definition of a 
%sandbox, it does not however describe anything more specific on the techniques. It does on the other hand broadly summarize some of the guarantees
%sandboxes provide and the ways they prove (or don't) them so it is to be considered from this standpoint.

Isolation is necessary to ensure that all interactions the sandbox makes pass through a given interface which can then be subject to policy enforcement. 

Policy enforcement is used to filter requests coming through the interface deciding what to do based on the policies enforced upon the domain. 
For example, a request to open a file is always done the same way but the access may be granted or denied.

Sandboxes employ a variety of techniques to enforce the isolation. These techniques vary based on what is being sandboxed and which hardware resources the sandbox has access to.
Some languages may provide features that allow for employing different isolation or policy enforcement mechanisms. 

Isolation between subjects within a single domain is not strictly necessary, as everything affected could be equally affected by all parties, while isolation between different domains is needed.

Having programs in separate domains with restricted rights means that a bug in one of them does not necessarily compromise the rest of the system and all data in it.
Separating programs like this reduces the impact any individual vulnerability has.

\section{Sandboxing in Operating Systems}
%\todo{I don't mention sort of esoteric OSes like the Hydra, SPIN, Midory which have the OS built around safety by enforcing language-based constraints}
Modern operating systems (OS) are typically split into the privileged \emph{kernel space} and the unprivileged, sandboxed \emph{user space}.
\emph{Kernel} is the core of the OS responsible for, among other things, sandboxing running user programs. 
%\todo{These concepts seem pretty commonly known as such I could just cite a random internet source. Like this \cite{Kernelspace}. Seems a bit pointless}

Hardware-based mechanisms are used to ensure isolation. Paging is used to map virtual memory and thus restrict access to physical memory. \cite{MicrosoftProcessThread, KernelMemoryManagement}
Hardware ring levels forbid unsafe instructions from being executed.
That makes using virtual memory mapping as a security measure possible as otherwise a program could rewrite its page table.
\cite{Intel}

\subsection{Processes}
	Operating systems employ processes as their memory/privilege isolation primitive. Each process has its own virtual memory which is by default not shared with other processes.
	A lower-level execution primitive are threads. But while threads can be used for further parallelization of workload, they do not belong to a different protection domain  
	--- individual threads of a single process have no isolation between them.  
	\cite{MicrosoftProcessThread, KernelMemoryManagement}

	When processes want to do more than just play around in their own address space, they must get access to resources from other processes or the kernel.
	But communication with other processes is done via mechanisms provided by the kernel. To summarize, all such access has to be done through the kernel.
	\cite{PosixIPC, WindowsIPC}

\subsection{Policy Enforcement}
	Even though each process is a protection domain by itself, it is by default not particularly restricted as it can affect the system as much as the user running it could. 

	All communication is restricted to the \emph{system call} interface --- requests that the kernel does something that a process cannot do by itself. 

	Just limiting what system calls are made using \emph{ptrace} or a kernel space module and filtering their arguments is sufficient 
	for creating a sandbox with fine-grained policy enforcement, though as Garfinkel \cite{garfinkelTraps} shows it comes with its share of difficulties. 

	There are other more sophisticated mechanisms in place allowing processes to opt-in to different limits on access to resources.
	These may include limiting process running time, restricting access to certain system calls altogether, and more. 
	\cite{MicrosoftProcessACL, MicrosoftProcessGiveUpRights, KernelSeccomp}

	Restricting this communication and filtering what should and should not be allowed is what lets us create a process-based sandbox, such as the one Chromium uses. 
	\cite{ChromiumSandbox}
\section{Virtualization-based Isolation}\label{VMS}

\emph{Virtualization} means creating virtual versions of physical resources and thus hiding the physical versions behind a controlled layer. 
A similar term is \emph{emulation} which is used when there is no physical version of the virtual resource and its functionality is simulated using software. \cite[p. 40-41]{OS9th} 

This allows for providing different instances of virtual resources to different components thus isolating them. 
An example would be the aforementioned virtual memory that processes are given access to instead of accessing physical memory directly.
	
\subsection{Virtual Machines}

	As virtual machines are virtualized computers, they should be perfectly isolated as they cannot just access the computer that is running them. 
	Such machines don't have to be running an operating system --- virtualizing the processor, memory, and potentially other peripheries is what allows the isolation to work. \cite{goonasekera2015libvm}
	
	It is to be noted that the sandboxed component still has to be granted an interface with which it can communicate with the outside world. 
	If the interface is not sufficiently restricted, the component can do as much harm as any other process.

	Sandboxing based on running a component in a minimalistic virtualized environment has been demonstrated. \cite{goonasekera2012hardware, goonasekera2015libvm, qiang2017libsec}
	
	Some programming languages are designed to be compiled for a specific virtual machine such as the Java Virtual Machine.
	Such machines do not require a real-world equivalent and may be fully emulated. \cite[p. 40-41]{OS9th}
	 
\section{Interpreters} \label{Interpreters}
	An interpreter is a program that executes the code it is given without first transforming it into another language as a compiler would.
	
	By definition, interpreters do not provide any security guarantees other than the ones the interpreted language gives.
	
\section{User Space Sandboxing Mechanisms}
If the OS does not provide sufficiently fine-grained control over permissions or the inter-process communication
overheads are considered too large \cite{EfficientSoftware-BasedFaultIsolation} other isolation or policy enforcement mechanisms may be used.
Ultimately, creating a sandbox running inside a process is necessary --- escaping the sandbox would be equal to taking control of the process and still having to deal with the restrictions imposed upon it by the operating system.

A major obstacle in this path is the OS taking a process as a single protection domain even though internally 
different libraries and plugins, which should not have equal rights, are used.

That means user space sandboxing must be implemented without using the hardware mechanisms that the OS uses and all the restrictions a process can opt-in to will apply to the entire process.
The OS-based restrictions should not be overlooked. If the sandbox were somehow bypassed, those would still be effective in restricting what damage can be done.

All the isolation mechanisms attempt to isolate parts of a program from each other --- essentially creating individual protection domains. 
Though if provided an insecure interface, they may still escape the sandbox and be used for malicious purposes.
\subsection{Software Fault Isolation}
	As Gang \cite{GangTanSFI} very well summarizes:
	
	\emph{Software Fault Isolation} is a technique based on running modified code. 
	Either by dynamically rewriting the binary code which is being executed or by statically in-lining checks into the entire program, 
	an input program is taken and transformed into a safer variant. \cite{GangTanSFI}
	
	This technique can be used to enforce both control flow and data flow integrity. For example, making sure only code inside the sandbox can be executed and only memory it owns can be read and written. \cite{GangTanSFI}

	It may be implemented as a compiler that adds the necessary checks into the resulting code. This method requires access to the source code. \cite{GangTanSFI}

	If compiled code is to be run directly, some form of a verifier is needed to ensure all checks have been inserted correctly. 
	Assuming the compiler is correctly implemented the issue can be sidestepped by only allowing running code which was compiled by the compiler beforehand.  \cite{GangTanSFI}
	
    Such guarantees come with some runtime speed overhead and with upkeep overhead as the compiler/rewriter/verifier need to be kept up to date. \cite{GangTanSFI}
	
	One prominent example of this approach is Native Client \cite{NaCl} which had been a part of the Chrome browser until 2019 when it got deprecated in favour of WebAssembly \cite{NaClmigrate}.
\subsection{Language-based Sandboxing}

	Language-based sandboxing is based on some features or properties of a language. 
	Much like how sandboxes restrict what resources can be accessed, languages may restrict by simply not having expressions to describe certain unsafe operations.
	
	It may also allow adding policies to using internal functions such as the ones from a language's standard library rather than only the system calls. 
	For example, in Java the Security Manager \cite{JEP411} (which is now deprecated) could be used to check which exact function calls lead to this restricted call and 
	decide if all of the callers had sufficient rights. Lua allows for wrapping of functions and restricting which parameters are OK and which are not or outright replacing the original functionality.
	
	Two features that must be implemented correctly are \emph{type safety} and \emph{memory safety}. 
	Otherwise, all restrictions a language imposes could be worked around.
	
	\subsubsection{Memory Safety}
		Memory safety is a difficult term to define --- commonly defined using the errors it prevents rather than the properties a memory-safe language should have. 
		\cite{MemorySafetyWeb}
		
		The paper \cite{MemorySafetyPaper} and article \cite{MemorySafetyWeb} attempt to formalise this, 
		but listing the errors it prevents gives us a more intuitive understanding of what it means.

		To give a simplified explanation, it should not be possible to access undefined memory. 
		Some common violations of this rule are \emph{use after free}, \emph{illegal free}, \emph{using uninitialized memory}, \emph{buffer overflow}, \emph{null pointer dereference}. \cite{MemorySafetyWeb}
		
	\subsubsection{Type Safety}	
	
		Type safety ensures that a programmer can trust that variable of a type always contains a valid value for that type. 
	
		Moreover, type safety ensures an operation with invalid types --- such as calling a function with incorrect parameters --- simply cannot be executed. 
		These guarantees may be enforced both statically at compile time and dynamically at run time.\cite{ProgrammingLanguagesSafety}
	
		A weak type system may be used to subvert memory safety as casting an integer to a pointer would allow for arbitrary memory access. 
		Without memory safety a type system cannot be fully trusted as the underlying value of a type could be easily overwritten to be invalid.
	
		\emph{Type confusion} is a bug where a program expects a value of a different type than it receives and continues operating on it with the assumption that it is correctly typed.
	\subsubsection{Achieving Isolation}	
		When memory safety and type safety get combined, a program cannot retrieve data from arbitrary memory locations.
		And if that is the case, all values and functions a program interacts with must come from some sort of interface described by the language.		
	
		This guarantee is sufficient for achieving isolation from the rest of the system,
		but the interface may introduce bugs that break the isolation 
		as the functions which are being interfaced may be written in an unsafe language and contain bugs or be inherently unsafe due to their functionality.

%		These functions may even be a part of the standard library of the language.
		A component, in this case, may be an entire program that is compiled directly into machine code (one such case is described in section \ref{WASM}). 
		In the case of a language running in an interpreter or a virtual machine, the entire interpreter may act as a component with restrictions imposed upon it rather than the language. This is further discussed in section \ref{StateBasedSbx}.
		
		While memory and type safety are still required, some languages may offer sandboxing or isolation as an integral part of the language.
		When only using memory and type safety, the entire runtime has to be constrained. 
		The languages allow for segregating protection domains based on lower-level primitives.
		
Java, for example, uses individual classes or more specifically protection domains as their primitive. Their protection domains are sets of classes with the same privileges.
		\cite[p. 2]{JavaSandboxAnalysis}
		
		In Lua, each function is ultimately a protection domain. This will be discussed later in section \ref{Closures}.
		
		Some languages may make isolation particularly hard by leaking potentially sensitive data from unexpected sources.
		A good example is Javascript in web browsers where --- unless strict mode is enabled --- the \codeHighlight{window} object is leaked through many different calls. \cite{AutoAnlyzisJS}		
		
	\subsubsection{Policy Enforcement}	
		Policy enforcement in language-based sandboxes differs from process-based sandboxes in a couple of ways. While the concepts are the same, details may differ.
		
		First and foremost, the controlled interface is often different. 
		Languages ship with their standard library which may be widely different from the system call interface. 
		As such, the restrictions must be tailored to this interface rather than the resources or system calls.
		
		Each value or function accessible with the language can be considered from a privilege standpoint --- deciding whether each domain should be able to use those resources. 
		The restrictions can of course be more fine-grained inside the functions should they be accessible at all.
		
		Some languages may allow for hiding or replacing functions, effectively removing them from the interface entirely.
		
		Policy enforcement may be a part of the language or its standard library. One such example is shown in section \ref{J+C}.
		
		Other mitigations come from language features themselves. For example, Lua strings are immutable. Once a string value is retrieved it cannot be changed, only manipulated to create a new string.
		When checking the contents of a string in C, its value may be changed from another thread after it has been validated --- a Time-of-check Time-of-use (TOCTOU) Race Condition \cite{CWE-367}.
		
\section{Sandboxing in Specific Languages}
This section covers some programming languages and sandboxing mechanics used in them.
Many other languages also allow for sandboxing in some way. 
	
Javascript is an example of a language where sandboxing is very useful for advertisements and other arbitrary third-party code.
The language is however not designed to make sandboxing easy. 
It can be encapsulated in an \emph{iframe} --- thus sidestepping the need for a language-based sandbox --- though that restricts it to a specific section of the page which may not always be wanted.
Otherwise sandboxing javascript is very complex as there are many subtle ways in which bypassing a sandbox may be possible. \cite{LanguageBasedJS, AutoAnlyzisJS}
	
\subsection{WebAssembly} \label{WASM}
	An interesting example of isolation implementation is WebAssembly (Wasm). 
	It is a `binary instruction format for a stack-based virtual machine' \cite{WasmWeb}, 
	meaning it is something akin to the machine instructions a processor uses.
	
	Wasm can be used as a compile target --- multiple languages can be compiled into Wasm. \cite{MDNWasm}.
	
	Wasm isolation is based on the concept of `Linear memory'. All memory accesses are bound to a restricted region indexed by a 32-bit integer. 
	As has been shown, a bug can result in the region's data being corrupted making it untrustworthy --- but only that region. 
	Barring a bug in the virtual machine or the provided API, no resources outside its own memory space can be affected. 
	A bug can result in malicious data being sent via the API as internally the memory safety is not guaranteed at all. 
	\cite{WASMmemExploits}
	
	Wasm does not provide any instructions for modifying the executable code. That means all functions have to be provided from outside of the sandbox.
	
	An example of how Wasm can be used for sandboxing is the Firefox browser which uses it via the RLBox library to sandbox some helper libraries. 
	It compiles to Wasm and then to machine code while retaining the security guarantees Wasm provides.
	\cite{RLBox_isolatio}
	\pagebreak
\subsection{Java and C\#}\label{J+C}
	While both Java and C\# provide type safety \cite{ProgrammingLanguagesSafety}, 
	sandboxing of partially trusted code has been discontinued \cite{MSDNCodeAccessSecurity,JEP411}
	as it is very hard to get right. \cite{JavaSandboxAnalysis,Microsoft_DevBlog_Porting}

	Both languages employed a rather complicated security policy manager based on checking the stack for which classes have been used in the calling chain and 
	determining if all of the classes have sufficient rights for the privileged operation. \cite{JavaSandboxAnalysis, MSDNCodeAccessSecurity}
	
	Instead, using the system-provided access control and isolation mechanics is now being recommended. \cite{MSDNCodeAccessSecurity}
	
	Java provides a module mechanism through which some sort of sandboxing may be possible \cite{Java_DevBlog_PostManager}, 
	but this method is not straightforward nor simple.
%\subsection{javascript}
%	\todo{Is what I have so far sufficient? Or should I cover JS as well?}
%	\todo{decide what you want here if anything at all}
%	sources:
%	https://www.usenix.org/conference/webapps12/technical-sessions/presentation/terrace
%	https://www.usenix.org/legacy/events/sec11/tech/full\_papers/Politz.pdf
%	https://theory.stanford.edu/~ataly/Papers/sp11.pdf
%	https://theory.stanford.edu/~ataly/Papers/esorics09-longVersion.pdf
%	https://theory.stanford.edu/~ataly/Papers/csf09-camera-ready.pdf
%	this paper while describing its own stuff gives a nice overview of everything
%	https://soft.vub.ac.be/Publications/2021/vub-soft-phd-21-06.pdf
% https://www.covert.io/research-papers/security/Cujo%20-%20Efficient%20detection%20and%20prevention%20of%20drive-by-download%20attacks.pdf
	
%	protection domains are mentioned also here, this specifically talks aoubout the access rights. Probabably nothing new:
%	https://homes.cs.washington.edu/~levy/capabook/Chapter1.pdf
	
%	as is nicely written in  https://theory.stanford.edu/~ataly/Papers/sp11.pdf
%	"if API methods are viewed as capabilities,
%then the API Confinement Problem is also known as the
%Overt Confinement Problem for Capabilities "

%Though I don't think I can fit that anywhere around here.

%Moreover the APIs can then be even more restricted so this is not necessarily the solution. There it had been used in the context of statically analyzing stuff. Though having an object in an enviroment and saying that if you ever get access to it problems arise is tempting. 